<?php
session_start();
if (isset($_SESSION['id'])) {
    $sessionID = $_SESSION['id'];
} else {
    header("location: ../index.php");
    exit();
}
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
          integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Redirection</title>
</head>
<body>

<?php require '../navigation/top-left-nav.php' ?>
<main>
    <div class="index_col_center">
        <h3><i class="fas fa-battery-empty"></i> IL Y A U PROBLEME ! <i class="fas fa-battery-full"></i></h3>
        <p> Vous allez être redirigé dans <span id="countdown">5</span> secondes.</p>
        <script type="text/javascript">
            //https://duckdev.com/blog/count-down-redirect-using-javascript/
            // Total seconds to wait
            var seconds = 5;
            function countdown() {
                seconds = seconds - 1;
                if (seconds < 0) {
                    // Chnage your redirection link here
                    window.location = "../index.php";
                } else {
                    // Update remaining seconds
                    document.getElementById("countdown").innerHTML = seconds;
                    // Count down using javascript
                    window.setTimeout("countdown()", 1000);
                }
            }
            // Run countdown function
            countdown();
        </script>
    </div>
</main>
</body>
</html>