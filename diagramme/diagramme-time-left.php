<?php
session_start();

if (isset($_SESSION['diagram'])) {
    $display = $_SESSION['diagram'];
} else {
    $display = 0;
}
if (isset($_SESSION['id'])) {
    $sessionID = $_SESSION['id'];
} else {
    $sessionID = NULL;
}
if (!empty($_POST["afficher"]) and preg_match('/[0-9]/', $_POST['authors'])) {
    $auth = (int)$_POST["authors"];
    $_SESSION['diagram'] = $auth;
    header("location: diagramme-time-left.php");
    exit;
}
?><!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
          integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p"
          crossorigin="anonymous"/>
    <title>Diagrammes</title>
    <script type="text/javascript" src="rpie.js"></script>
    <script>
        document.querySelectorAll('.table-responsive').forEach(function (table) {
            let labels = Array.from(table.querySelectorAll('th')).map(function (th) {
                return th.innerText
            })
            table.querySelectorAll('td').forEach(function (td, i) {
                td.setAttribute('data-label', labels[i % labels.length])
            })
        })
    </script>
</head>
<body>
<?php
require '../navigation/top-left-nav.php';
require "../sql/connexion.php";
?>
<main>
    <div class="index_col_center-diagramme">
        <h2><a href="diagramme-time-left.php">Diagrammes</a></h2>

        <!-- selection du nom de l'auteur pour ensuite afficher son diagramme-->
        <form action="diagramme-time-left.php" method="post">
            <label for="authors">Choisir d'afficher pour: </label>
            <!-- req sql pour récupérarer la table "author"-->
            <?php $authorSelect = $dbh->prepare("SELECT * FROM author");
            $authorSelect->execute();
            //  création du select avec les options:
            echo "<select name='authors' id='authors'>";
            $tous = 0; ?>
            <option value="<?php echo (int)$tous ?>">Tous les auteurs</option>
            <?php while ($authorList = $authorSelect->fetch()) { ?>
                <option value="<?php echo (int)($authorList["id"]); ?> ">
                    <?php echo htmlspecialchars($authorList["name"]); ?>
                </option>
            <?php }
            echo "</select>"; ?>
            <input type="submit" name="afficher" value="Afficher">
        </form>
        <!--    récupération de la valeur du select  | 0 = tous |
        chiffre (1,2,3...etc.) = id des auteurs dans la database -->
        <br>
        <!--        affichage du nom d'auteur dans un h4-->
        <h4> Reste à faire de
            <?php if ($display == 0) {
                echo " tous les auteurs";
            } else {
                $h3 = $dbh->prepare("SELECT name FROM author WHERE id = ?");
                $h3->execute(array(
                    $display,
                ));
                $name = $h3->fetch();
                echo " " . $name['name'];
            } ?></h4>

        <!--    ligne rassemblant les trois diagrammes-->
        <div class="diaTime">
            <!--        diagramme de gauche-->
            <div class="pie-left">
                <div class="pieBlock">
                    <?php
                    $jour = date("d-m-Y");
                    $d9 = strtotime($jour . "+ 35 days");
                    ?>
                    <div class="text-justifty"> Date d'échéance dépassée <i class="fas fa-level-down-alt"></i></div>
                    <?php
                    include 'pie-left.php';
                    ?></div>

                <?php
                include 'under-pie-left.php';
                ?>
            </div>
            <!--        diagramme central en bar-->
            <div class="dia-size-center">
                <div class="dia-center">
                    <?php include 'diagram-central.php' ?>
                </div>
            </div>
            <!--        diagramme de droite -->
            <div class="pie-right">
                <div class="pieBlock">
                    <?php
                    $jour = date("d-m-Y");
                    $d9 = strtotime($jour . "+ 39 days");
                    ?>
                    <div class="text-justifty">Date d'échéance à partir du<br>
                        <?php echo date("d-m-Y", $d9); ?><i class="fas fa-level-down-alt"></i></div>
                    <?php include 'pie-right.php' ?>
                </div>
                <?php include 'under-pie-right.php' ?>

            </div>
        </div>

        <!--    sous les diagrammes, le tableau des RAFs-->
        <?php
        //Selection de tout ce qu'il y a dans la database: table -> restes à faire
        //Première req: Pour tout le monde
        //Deuxième req: Par auteurs
        if (($display) == 0) {
            $displayRaf = $dbh->prepare("SELECT * FROM RAF");
            $displayRaf->execute();
        } elseif (($display)) {
            $displayRaf = $dbh->prepare("SELECT * FROM RAF WHERE author_id = ? ");
            $displayRaf->execute(array(
                $display,
            ));
        }

        echo "<table class='table-responsive'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th class='td1'>" . "Auteur" . "</th>";
        echo "<th class='td2'>" . "Durée" . "</th>";
        echo "<th class='td3'>  " . "Dans le projet" . "</th>";
        echo "<th class='td7'>  " . "Priorité" . "</th>";
        echo "<th class='td4'>" . "Déscription" . "</th>";
        echo "<th class='td5' >" . "Observation" . "</th>";
        echo "<th class='td6' >" . "1/3" . "</th>";
        echo "<th class='td7' >" . "2/3" . "</th>";
        echo "<th class='td8' >" . "Fini" . "</th>";
        echo "<th class='td9' >" . "Date de fin" . "</th>";
        echo "<th class='td10' >" . "Jours Restant" . "</th>";
        echo "</tr>";
        echo "</thead><tbody>";

        while ($display = $displayRaf->fetch(PDO::FETCH_ASSOC)) {

//Récupération du nom de l'auteur
            $authorNames = $dbh->prepare("SELECT * FROM author WHERE id = ?");
            $authorNames->execute(array(
                $display['author_id'],
            ));
            $authorName = $authorNames->fetch();
//Récupération de nom du projet
            $includeNames = $dbh->prepare("SELECT * FROM includeInProjects WHERE id = ?");
            $includeNames->execute(array(
                $display['includeInProject_id'],
            ));
            $includeName = $includeNames->fetch();

//calcul jour restant avant la deadline
            $today = date("Y-n-j");
            $origin = date_create($today);
            $target = date_create($display['deadline']);
            $interval = date_diff($origin, $target, false);
            $dDay = $interval->format('%R%a days');


//Avancement 1/3
            if ($display['un_tiers'] == 1) {
                $dOThird = '<i class="fas fa-check-circle fasCheckDia" style="line-height: inherit"></i>';
            } else {
                $dOThird = "";
            }
//Avancement 2/3
            if ($display['deux_tiers'] == 1) {
                $dTThird = '<i class="fas fa-check-circle fasCheckDia"></i>';
            } else {
                $dTThird = "";
            }

            //Avancement fini
            if ($display['trois_tiers'] == 1) {
                $finishThird = '<i class="fas fa-check-circle fasCheckDia" style="line-height: inherit"></i>';
            } else {
                $finishThird = "";
            }

            //Couleur de fond des jours restants
            if ($dDay > -1 && $dDay < 3) {
                $tdColor = "<td data-label='Jours restants' class='td10 td-yellow'>";
            } elseif ($dDay > 2) {
                $tdColor = "<td data-label='Jours restants' class='td10 td-green'>";
            } else {
                $tdColor = "<td data-label='Jours restants' class='td10 td-red'>";
            }

            $bg = $includeName["color"];

            //Tableau avec le reste à faire | pour tous ou auteur par auteur
            echo "<tr>";
            echo "<td class='td1' data-label='Auteur'>" . $authorName["name"] . "</td>";

            echo "<td class='td2' data-label='Durée'>" . str_replace('.', 'h', $display['duration']) . "</td>";

            if($includeName["name"] == "aucune"){
                echo "<td class='td3' data-label='Dans le projet' style='background-color: $bg ; color: #fff'><b>" . $includeName["name"] . "</b></td>";

            } else {
                echo "<td class='td3' data-label='Dans le projet' style='background-color: $bg ;'><b>" . $includeName["name"] . "</b></td>";
            }
            echo "<td class='td2' data-label='Priorité'>" . $display['priority'] . "</td>";
            echo "<td class='td4' data-label='Déscription'>" . $display['description'] . "</td>";

            echo "<td class='td5' data-label='Observation'>" . $display['observation'] . "</td>";

            echo "<td class='td6' data-label=1/3''>" . $dOThird . "</td>";
            echo "<td class='td7' data-label='2/3'>" . $dTThird . "</td>";
            echo "<td class='td8' data-label='Fini'>" . $finishThird . "</td>";

            echo "<td class='td9' data-label='Date de fin'>" . $display['deadline'] . "</td>";

            echo " $tdColor" . $dDay . "</td>";
            echo "</tr>";
        }
        echo "</tbody>";
        echo "</table>";
        ?>
    </div>
</main>
</body>
</html>
<?php unset($_SESSION['diagram']) ?>