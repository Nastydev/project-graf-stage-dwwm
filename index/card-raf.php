<?php
if (isset($_SESSION['id'])) {
    $authorId = $_SESSION['authorId'];
    $authorRole = $_SESSION['role'];
} else {
    $authorId = NULL;
    $authorRole = NULL;
}

// récupération de l'id de l'auteur pour n'afficher que son reste à faire si il clic sur son nom en haut à droite.
if (isset($_GET['id']) && preg_match('/[0-9]/', $_GET['id'])) {
    $_GET['id'];

    $displayRaf = $dbh->prepare("SELECT * FROM RAF WHERE author_id = :id");
    $displayRaf->execute(array(
       "id" => $_GET['id']
));
} //sinon affichage du reste à faire pour tout le monde
else {
    $displayRaf = $dbh->prepare("SELECT * FROM RAF");
    $displayRaf->execute();
}



//début de l'affichage par reste à faire
while ($display = $displayRaf->fetch(PDO::FETCH_ASSOC)) {

    $authorNames = $dbh->prepare("SELECT * FROM author WHERE id = :id");
    $authorNames->execute(array(
           "id" => $display['author_id'],
    ));
    $authorName = $authorNames->fetch();

    $includeNames = $dbh->prepare("SELECT * FROM includeInProjects WHERE id = :idInclude ");
    $includeNames->execute(array(
            "idInclude" => $display['includeInProject_id'],
    ));
    $includeName = $includeNames->fetch();

    //calcul jour restant avant la deadline
    $today = date("Y-n-j");
    $origin = date_create($today);
    $target = date_create($display['deadline']);
    $interval = date_diff($origin, $target, false);
    $dDay = $interval->format('%R%a days');

    //couleur des bordures selon la priorité
    if ($display['priority'] == 4) {
        $colorBorder = '<div class="display_raf" style="border-color: red; display: inline-table;">';
    } elseif ($display['priority'] == 3) {
        $colorBorder = '<div class="display_raf" style="border-color:  orangered ; display: inline-table">';
    } elseif ($display['priority'] == 2) {
        $colorBorder = '<div class="display_raf" style="border-color:  yellow; display: inline-table">';
    } else {
        $colorBorder = '<div class="display_raf" style="border-color:  green; display: inline-table">';
    }

    //background des raf faient à 100%
    if ($display['trois_tiers'] == 1) {
        $colorBorder = '<div class="display_raf" style="border-color: #0066ff ; background-color: #0066ff; display: inline-table">';

    }

    echo $colorBorder; //début de la div avec la bordure de couleur par prioritée

    //récupération de l'id du reste à faire pour l'edition et la suppression du/des restes à faire.
    $rafId = $display['id'];
    $rafAuthorID = $display['author_id'];
    ?>
    <div class="displaytitle">
        <!--  si l'utilisateur est connecté il verra les icones modifier et supprimer, sinon il ne se passera rien-->
        <?php
         /*
         * Bouton: EDIT & DELETE
         * Récupération de l'id de la personne connecté et du role de cette personne.
         * Si dans la table RAF il y a un reste à faire pour cette personne -> affichage edit / delete
         * MAIS seulement pour cette personne !
         * Par contre, si son role est égale à 1 -> affichage edit / delete, sur toutes les cates RAF
        */
        if ($authorId == $rafAuthorID or $authorRole == 1) {
            include 'edit-delete-raf/nav_edition.php';
        }

        echo '<div class="displayName"><b>' . $authorName["name"] . "</b></div></div>";

        if ($display['trois_tiers'] == 1){
            echo '<div class="displayDuration">' .' '. "</div>";
        } else{
            echo '<div class="displayDuration">' . '<div class="displayBaseInfo">' . "Durée: " . '</div>' . strtr($display['duration'], ".", "h") . "</div>";
}
    $iColor = $includeName["color"]; //récupération de la couleur de la catégorie
    if ($includeName["name"] == "aucune"){
        $nameCategory = "";
    } else {
        $nameCategory = $includeName["name"];
    }
        ?>   <div class="displayProject" style="border-bottom: 2px solid <?php echo $iColor ?>;font-weight: bold;"><?php echo  $nameCategory ?> </div>
    <?php
    //si 100% couleur de description en blanc, sinon en bleu
    if ($display['trois_tiers'] == 1){
        $spanDescription = "style='color: #000';";
    } else{
        $spanDescription = "style='color: #0066ff';";
    }
    echo '<div class="displayDescription">' . "<b><span $spanDescription>Description:</span></b> " . $display['description'] . "</div>";

    //si 100% pas de display de l'observation
     if ($display['trois_tiers'] == 1){
    echo '<div class="displayObs">' . '<div class="displayBaseInfo">' . " " . '</div>' . $display['observation'] . "</div>";

     } else {
    echo '<div class="displayObs">' . '<div class="displayBaseInfo">' . "Observation: " . '</div><b>' . $display['observation'] . "</b></div>";
     }

    //affichage d'une barre d'avancement en 3 étapes 33%, 66% et 100%
    if ($display['un_tiers'] == 0 and $display['deux_tiers'] == 0) {
        ?>
        <div class="bg_third"></div>
        <div class="bg0text">0%</div>

        <?php
    }
    if ($display['un_tiers'] == 1 and $display['deux_tiers'] == 0) {
        ?>
        <div class="bg_third"></div>
        <div class="bg33text">33%</div>
        <?php
    }
    if ($display['un_tiers'] == 1 and $display['deux_tiers'] == 1 and $display['trois_tiers'] == 0) {
        ?>
        <div class="bg_third"></div>
        <div class="bg66text">66%</div>
        <?php
    }
    if ($display['un_tiers'] == 1 and $display['deux_tiers'] == 1 and $display['trois_tiers'] == 1) {
        ?>
        <div class="bg_thirdOVER">Tâche terminée</div>
        <?php
    }
    echo '<div class="displayDeadline">' . $display['deadline'] . "</div>";

    //            changement de couleur quand la date est dépassé et en négatif
    if ($dDay <= 0) {
        $dDayColor = "color:red;";
    } else {
        $dDayColor = "color:#000;";
    }
    echo '<div class="displayDDay" style="' . $dDayColor . '">' . $dDay . "</div>";

    echo "</div>";
}
?>