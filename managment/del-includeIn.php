<?php
session_start();
if (isset($_SESSION['id'])) {
    $sessionID = $_SESSION['id'];
} else {
    $sessionID = NULL;
}

require '../sql/connexion.php';

if (isset($_GET['id']) && preg_match('/[0-9]/', $_GET['id'])) {
    $id = $_GET['id'];

    $reqDelIncl = $dbh->prepare("DELETE FROM `includeInProjects` WHERE `id` = ?");
    $reqDelIncl->execute(array(
        $id,
        ));

    header("location: req-includeInProj.php");
    exit();

}  else {
    header('location: ../redirection/problem.php');
    exit();
}

?>
