<?php
session_start();
if (isset($_SESSION['id'])) {
    $sessionID = $_SESSION['id'];
} else {
    $sessionID = NULL;
}

require '../sql/connexion.php';

if (isset($_GET['id']) && preg_match('/[0-9]/', $_GET['id'])) {
    $id = $_GET['id'];

    $reqDeleteRaf = $dbh->prepare("DELETE FROM `RAF` WHERE author_id = ?");
    $reqDeleteRaf->execute(array(
        $id,
    ));

    $reqDeleteMember = $dbh->prepare("DELETE FROM `membre` WHERE author_id = ?");
    $reqDeleteMember->execute(array(
        $id,
    ));

    $reqDeleteAuthor = $dbh->prepare("DELETE FROM `author` WHERE id = ? ");
    $reqDeleteAuthor->execute(array(
        $id,
    ));

    if ($id != $sessionID) {
        header("location: req-author.php");
        exit();
    }

}  else {
    header('location: ../redirection/problem.php');
    exit();
}

?>
