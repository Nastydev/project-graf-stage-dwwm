<?php
session_start();
if (isset($_SESSION['id'])) {
    $sessionID = $_SESSION['id'];
} else {
    $sessionID = NULL;
}
require '../sql/connexion.php';

if (isset($_GET['id']) && preg_match('/[0-9]/', $_GET['id'])) {
    $id = $_GET['id'];
} else {
    header('location: ../redirection/problem.php');
    exit();
}
$reqRoles = $dbh->prepare("SELECT * FROM membre WHERE id = ?");
$reqRoles->execute(array(
        $id,
));

$reqRole = $reqRoles->fetch();

if (isset($_POST['envoyer'])) {
    if (!empty($_POST['role']) and (int)$_POST['role']) {
        $role = $_POST['role'];

        $reqUpdateName = $dbh->prepare("UPDATE `membre` SET `roles_id` = ? WHERE `id` = ?");
        $reqUpdateName->execute(array(
                $role,
                $id,));

        header("location:req-author.php");
        exit();
    } else {
        header('location: ../redirection/problem.php');
        exit();
    }
}
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../css/style.css">
    <title>Modification du nom d'auteur</title>
</head>
<body>

<?php
require '../navigation/top-left-nav.php';

?>
<main>
    <div class="index_col_center">
        <h2>Modification</h2>
        <form action="" method="post">
            <label for="role">
                Les autorisations: <br>

                <b> 1 : Administrateur </b>[Personnes ayant tous les droits] <br>
                <b> 2 : Autorisé </b>[Peux lire et créer et modifier le reste à faire, mais aucuns accès de modification
                des catégories de projet, auteurs ...etc]<br>
                <b> 3 : non-autoriser</b>[Peux juste lire sans créer, modifier ou supprimer.]<br>

            </label>
            <input type="number" name="role" id="role" value="<?php echo $reqRole['roles_id'] ?>">

            <input type="submit" name="envoyer" value="Enregistrer">
        </form>
    </div>
</main>
</body>
</html>