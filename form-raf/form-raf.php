<?php
session_start();
if (isset($_SESSION['id'])) {
    $sessionID = $_SESSION['id'];
    $role = $_SESSION['role'];
} else {
    header("location: ../index.php");
    exit();
}


require '../sql/connexion.php';

$errorRaf = [];

//date de la journée
$date = date("Y-m-d");

if (isset($_POST["envoyer"])) {
    /**  fonction pour vérifier:
     * trim() = Supprime les espaces
     * stripslashes () = Supprime les antislashs d'une chaîne
     * htmlspecialchars =  Convertit les caractères spéciaux en entités HTML
     * preg_replace = suppression des espaces en trop
     */
    function verification($donnees)
    {
        $donnees = trim($donnees);
        $donnees = stripslashes($donnees);
        $donnees = htmlspecialchars($donnees);
        $donnees = preg_replace("/\s+/", " ", $donnees);
        return $donnees;
    }

    $author = verification($_POST["author"]);
    $description = verification($_POST["description"]);
    $hours = $_POST["hours"];
    $minutes = $_POST["minutes"];
    $priority = $_POST["priority"];
    $includeIn = verification($_POST["includeIn"]);
    $deadline = $_POST["deadline"];
    $oneThird = isset($_POST['one_third']) ? $_POST['one_third'] : 0;
    $twoThird = isset($_POST['two_third']) ? $_POST['two_third'] : 0;
    $observation = verification($_POST["observation"]);

    //vérification que le nom d'auteur sélectionné, existe dans la base de données
    $reqAuhorsNames = $dbh->prepare("SELECT * FROM author WHERE `name` = ?");
    $reqAuhorsNames->execute(array(
            $author,
        ));
    $authorNames = $reqAuhorsNames->rowCount();

    //vérification que le nom de includeInProject sélectionné, existe dans la base de données
    $reqIncProjectNames = $dbh->prepare("SELECT * FROM includeInProjects WHERE `name` = ?");
    $reqIncProjectNames->execute(array(
            $includeIn,
    ));
    $incProjectName = $reqIncProjectNames->rowCount();

    if (empty($author)) {
        $errorRaf[] = "Vous devez entrer un nom d'ateur. Si vous n'en avez pas créer un compte et informer votre chez de projet.";
    }
    if (!empty($author) and !preg_match("/^[a-zA-Z0-9 ]+$/", $author)) {
        $errorRaf[] = "Caractère non autorisé dans le nom de l'auteur";
    }

    if (!empty($author) and $authorNames != 1) {
        $errorRaf[] = "Veuillez créer un compte avec un nom d'auteur";
    }

    if (empty($description)) {
        $errorRaf[] = "Description obligatoire";
    }
    if (empty($hours) and empty($minutes)) {
        $errorRaf[] = "La durée est obligatoire";
    }

    if (!is_numeric($hours) and !is_numeric($minutes)) {
        $errorRaf[] = "L'heure doit être ajouté avec des chiffres";
    }

    if ($minutes > 59) {
        $errorRaf[] = "Les minutes ne peuvent pas être supérieur à 59";
    }

    if ($minutes < 0 OR $hours < 0) {
        $errorRaf[] = "La durée ne peux pas contenir de chiffres négatifs";
    }

    if ($hours >= 1000) {
        $errorRaf[] = "La durée ne peux pas accéder 1000 heures";
    }
    if (empty($priority)) {
        $errorRaf[] = "Priorité obligatoire";
    }

    if (!empty($priority) and !is_numeric($priority)) {
        $errorRaf[] = "La priorité doit être impérativement un chiffre";
    }

    if (!empty($priority) and !preg_match("/^[0-4]+$/", $priority)) {
        $errorRaf[] = "Le chiffre de la priorité doit être compris entre 0 et 4";
    }

    if (!empty($includeIn) and !preg_match("/^[\-a-zA-Z0-9éèêëàçùôö' ]+$/", $includeIn)) {
        $errorRaf[] = "Caractère non autorisé dans le nom de l'ajout d'un catégorie d'action";
    }

    if (!empty($includeIn) and $incProjectName != 1) {
        $errorRaf[] = "Veuillez choisir un nom de catégorie de projet valide";
    }

    if (empty($deadline)) {
        $errorRaf[] = "La date d'échéance est obligatoire";
    }
    if (!empty($deadline) and !preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}\z/', $deadline)) {
        $errorRaf[] = "La date d'échéance doit être au formation JJ/MM/AAAA";
    }

    if (!preg_match('/^[0-1]$/', $oneThird)) {
        $errorRaf[] = "Il y a un problème avec l'avancement";
    }

    if (!preg_match('/^[0-1]$/', $twoThird)) {
        $errorRaf[] = "Il y a un problème avec l'avancement";
    }


    if (!empty($errorRaf)) {
        $_SESSION['errorRAF'] = $errorRaf;
    } else {
        if (strlen($minutes) == 1) {
            $min = 0 . $minutes;
        } else {
            $min = $minutes;
        }

        $duration = $hours . "." . $min;
        //recupération de l'id "author"
        $checkAuthors = $dbh->prepare("SELECT `id` FROM author WHERE name =?");
        $checkAuthors->execute(array(
            $_POST["author"],
        ));
        $checkAuthor = $checkAuthors->fetch();
        $checkAuthors->closeCursor();
//      verification de l'écriture "include dans le projet
        if (!empty ($includeIn) and preg_match("/^[\-a-zA-Z0-9éèêëàçùôö' ]+$/", $includeIn)) {
            //recupération de l'id "includeInProject" ////// pas besoin de else car peu être null :!
            $reqIds = $dbh->prepare("SELECT `id` FROM includeInProjects WHERE name =?");
            $reqIds->execute(array(
                $_POST["includeIn"],
            ));
            $reqId = $reqIds->fetch();
        }

//      si observation est rempli ↓
        if (!empty($_POST["observation"])) {
            $observation = verification($_POST["observation"]);
            //si observation est vide ↓
        }

//      si la case du 2/3 est cochée il faudra aussi que 1/3 soit marquée comme fait
        if ($twoThird == 1) {
            $oneThird = 1;
        }


        $reqRaf = $dbh->prepare
        ("INSERT INTO
    `RAF`(`author_id`, `description`, `duration`, `priority`, `deadline`, `includeInProject_id`, `un_tiers`, `deux_tiers`, `observation`)
    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);");
        $reqRaf->execute(array(
            $checkAuthor['id'],
            $description,
            $duration,
            $priority,
            $deadline,
            $reqId['id'],
            $oneThird,
            $twoThird,
            $observation,
        ));
        header("Location: ../redirection/redirection-newRAF.php");
        exit();
    }
}

?>
    <!doctype html>
    <html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="../css/style.css">
        <title>Nouveau RAF</title>
    </head>
    <body>
    <?php require '../navigation/top-left-nav.php' ?>
    <main>
        <div class="index_col_center">
            <h2>Nouvelle tâche du Reste A Faire</h2>

            <?php
            if (isset($_SESSION['errorRAF'])) {
                $a = $_SESSION['errorRAF'];
                echo '<div class="error">';
                echo "<div>" . implode('<br>', $a) . "</div>";
                echo '</div>';
            }
            ?>
            <br>
            <br>
            <div class="responsive-block-form">
                <?php
                //recupération de l'affichage de l'id du role
                if ($_SESSION['role'] != 3):
                ?>
                <form action="form-raf.php" method="post">
                    <div class="col_form_left">
                        <div class="label-input-raf-form">
                            <label for="author"><b>Choisir votre nom d'auteur*</b></label>
                            <br>
                            <?php
                            $authorSelect = $dbh->prepare("SELECT name FROM author");
                            $authorSelect->execute();
                            echo "<select name='author' id='author'>";
                            while ($authorList = $authorSelect->fetch()) { ?>
                                <option value="<?php echo htmlspecialchars($authorList["name"]); ?> ">
                                    <?php echo htmlspecialchars($authorList["name"]); ?>
                                </option>

                            <?php }
                            echo "</select>";
                            ?>
                        </div>
                        <div class="label-input-raf-form">
                            <label for="description"><b>Déscription de l'action prévue* </b></label>
                            <br>
                            <textarea rows="4" cols="54" name="description" id="description"></textarea>
                        </div>

                        <div class="label-input-raf-form">
                            <label><b>Durée nécessaire*</b></label>
                            <br> <label for='hours'>heure(s):</label>
                            <input id='hours' name='hours' type='number' size="2" min='0' value="0">

                            <input id='minutes' name='minutes' type='number' max='59' size="2" value="0">
                            <label id='minutes'>:minutes</label>
                        </div>

                        <div class="label-input-raf-form">
                            <label for="priority"><b>Priorité de 1 à 4*</b><br>
                                (Sachant que 4 est le plus haut niveau de priorité) </label>
                            <br>
                            <input type="number" min="1" max="4" name="priority" id="priority">
                        </div>

                    </div>
                    <div class="col_form_right">
                        <div class="label-input-raf-form">
                            <label><b>Dans quelle catégorie inclure l'action?</b></label>
                            <br>
                            <?php
                            $includeSelect = $dbh->prepare("SELECT * FROM `includeInProjects`");
                            $includeSelect->execute();

                            echo '<select name="includeIn">';

                            while ($includeList = $includeSelect->fetch()) { ?>
                            <option value="<?php echo htmlentities($includeList['name']) ?>">
                                <?php echo htmlentities($includeList['name']) . '</option>';

                                }
                                echo '</select>' ?>
                        </div>

                        <div class="label-input-raf-form">
                            <label for="deadline"><b>Date d'échéance*</b></label>
                            <input type="date" name="deadline" id="deadline">
                        </div>

                        <div class="label-input-raf-form">
                            <label><b>Avancement actuel</b></label> <br>
                            <input type="checkbox" id="one_third" name="one_third" value="1">
                            <label for="one_third"> 1/3 </label><br>
                            <input type="checkbox" id="two_third" name="two_third" value="1">
                            <label for="two_third"> 2/3 </label><br>
                        </div>

                        <div class="label-input-raf-form">
                            <label for="observation"><b>Observation(s)</b><br>
                                Les précisions sur la réalisartion doivent rester dans la première case. <br>
                                Ici il s'agit juste de préciser la raison éventuelle à prendre en compte sur la date
                                butoir.
                                <br>
                                Exemple: est-elle non negociable?
                                <br>(L'observation empêchera toutes modifications du reste à faire)</label>
                            <br>
                            <textarea rows="4" cols="54" name="observation" id="observation"></textarea>
                        </div>
                    </div>

                    <div class="submit label-input-raf-form">
                        <label><i>Les champs marqués d'une * sont obligatoires</i></label><br>
                        <input type="submit" name="envoyer" value="Entrer les données">
                    </div>
                </form>
            </div>
        <br>
        <br>

        <?php else:
            echo 'Vous n\'êtes pas autorisé à poster';
        endif;
        ?>

        </div>
    </main>
    </body>
    </html>
<?php unset($_SESSION['errorRAF']) ?>