<?php
if (isset($_SESSION['id'])) {
    $authorId = $_SESSION['authorId'];
} else {
    $authorId = NULL;
}
?>
<header>
    <div class="index-top-nav">
        <h1><a href="../index.php">Projet GRAF</a></h1>
        <h1 id="responsiveH1"><a href="../index.php"><img src="../images/home.png" alt="Acceuil"></a></h1>
        <!------------------------ TOP SIDE ----------------------------->
        <div class="connexion">
            <?php
            if (isset($sessionID)): ?>
                <div class="btn-nav">
                    <?php
                    echo '<div class="btn-nav">';
                    echo '<a href="../index.php?id=' . $authorId . '"' . ' class="co1">' . $_SESSION['authorName'] . "</a>";
                    echo '<a href="../index.php?id=' . $authorId . '"' . ' class="co1">'
                        . '<img src="../images/logged.png" alt="#" class="connexion-img">'
                        . '<img src="../images/loggedhover.png" alt="#" class="connexion-img-hover">' .
                        '</a>';
                    echo '</div>';
                    ?>
                </div>
                <div class="btn-nav">
                    <a href="../membres/logout.php">Déconnexion</a>
                    <a href="../membres/logout.php" title="Déconnexion">
                        <img src="../images/logandout.png" alt="Déconnexion" class="connexion-img">
                        <img src="../images/logouthover.png" alt="Déconnexion" class="connexion-img-hover">
                    </a>
                </div>
            <?php else: ?>
                <div class="btn-nav">
                    <a href="../membres/form-inscription.php">Nouveau</a>
                    <a href="../membres/form-inscription.php" title="Nouveau membre">
                        <img src="../images/join.png" alt="Nouveau membre" class="connexion-img">
                        <img src="../images/joinhover.png" alt="Nouveau membre" class="connexion-img-hover">
                    </a>
                </div>
                <div class="btn-nav">
                    <a href="../membres/login.php">Connexion</a>
                    <a href="../membres/login.php" title="Connexion">
                        <img src="../images/logandout.png" alt="Connexion" class="connexion-img">
                        <img src="../images/loginhover.png" alt="Connexion" class="connexion-img-hover">
                    </a>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <!------------------------ LEFT SIDE ----------------------------->
    <?php
    if (isset($sessionID)): ?>
    <div class="index_nav_col_left" id="responsiveNavLeft-Co">
        <ul class="left_nav_top_ul" id="responsive-Co-UL">
            <li id="responsiveCoRAF"><a href="../form-raf/form-raf.php">Nouveau reste à faire</a></li>
            <li id="responsiveCoCP"><a href="../includeInProject/includeInProject.php">Nouvelle catégorie de projet</a>
            </li>
            <li id="responsiveCoImg">
                <a href="#">
                    <img src="../images/clic.png" alt="#" class="responsiveCoImg">
                </a></li>
        </ul>
        <ul class="left_nav_top_ul">
            <li id="responsiveCoMA"><a href="../managment/req-author.php">Manage auteurs</a></li>
            <li id="responsiveCoMCP"><a href="../managment/req-includeInProj.php">Manage catégories projet</a></li>
        </ul>
        <?php else : ?>
        <div class="index_nav_col_left" id="responsiveNavLeft">
            <ul class="left_nav_top_ul-Off" id="responsive-Not-Co-UL">
                <li id="responsiveNotCo">Veuillez-vous connecter pour voir les options</li>
                <li id="responsiveNotCoImg">
                    <a href="#">
                        <img src="../images/clic.png" alt="#" class="responsiveNotCoImg">
                    </a></li>
                <?php endif; ?>
            </ul>
            <ul class="left_nav_top_ul" id="responsivediaNav">
                <li><a href="../diagramme/diagramme-time-left.php">Diagramme des RAF</a></li>
            </ul>
        </div>

        <?php if (isset($_SESSION['id'])): ?>
            <!--Partie gauche pour les personnes connectés -->
            <script>
            var responsiveCoUl   = document.getElementById('responsive-Co-UL')
            var responsiveCoImg   = document.getElementById('responsiveCoImg')
            var responsiveCoRaf   = document.getElementById('responsiveCoRAF')
            var responsiveCoCp   = document.getElementById('responsiveCoCP')
            var responsiveCoMa   = document.getElementById('responsiveCoMA')
            var responsiveCoMcp   = document.getElementById('responsiveCoMCP')
            var responsiveCoNav   = document.getElementById('responsivediaNav')
            var responsiveCoNavLeft   = document.getElementById('responsiveNavLeft-Co')

                function myFunction(x) {

                    if (x.matches) {

                        /** si la fenêtre fait moins de 1366px:
                         * pas de couleur blanche dans les ul
                         * display des li sur none
                         * et display de l'image sur block
                         * taille de la div a 45px */
                       responsiveCoUl.style.backgroundColor = "unset";
                        responsiveCoImg.style.display = "block";
                        responsiveCoRaf .style.display = "none";
                        responsiveCoCp.style.display = "none";
                        responsiveCoMa.style.display = "none";
                        responsiveCoMcp.style.display = "none";
                        responsiveCoNav.style.display = "none";
                        responsiveCoNavLeft.style.width = 45 + 'px';

                        // au survol de la barre de gauche
                        /** au survol de la souris
                         * la barre reprend ses 220 de pixels
                         * background des ul en blanc
                         * les textes des li sont affichés en block
                         * display de l'image sur none */
                        responsiveCoNavLeft.onmouseover = function () {
                            this.style.width = 220 + 'px';
                            responsiveCoUl.style.backgroundColor = "#fff";
                            responsiveCoImg.style.display = "none";
                            responsiveCoRaf .style.display = "block";
                            responsiveCoCp.style.display = "block";
                            responsiveCoMa.style.display = "block";
                            responsiveCoMcp.style.display = "block";
                            responsiveCoNav.style.display = "block";
                            responsiveCoNavLeft.style.width = 220 + 'px';

                        }
                        // Quand la souris ne survol plus la barre de gauche
                        /** quand la souris quitte le hover ou après clic sur un lien
                         * la couleurs et les li disparaissent
                         * et l'image reviens en display block*/
                        responsiveCoNavLeft.onmouseout = function () {
                            this.style.width = 45 + 'px';
                            responsiveCoUl.style.backgroundColor = "unset";
                            responsiveCoImg.style.display = "block";
                            responsiveCoRaf .style.display = "none";
                            responsiveCoCp.style.display = "none";
                            responsiveCoMa.style.display = "none";
                            responsiveCoMcp.style.display = "none";
                            responsiveCoNav.style.display = "none";
                            responsiveCoNavLeft.style.width = 45 + 'px';
                        }

                    } else {
                        // si la taille de la fenêtre est supérieur à 1366px
                        /** background blanc dans les ul
                         * affichage li en block
                         * image sur display none
                         * taille de la barre de gauche à 220 px, et hover ou non la taille reste à 220px */
                        responsiveCoUl.style.backgroundColor = "#fff";
                        responsiveCoImg.style.display = "none";
                        responsiveCoRaf .style.display = "block";
                        responsiveCoCp.style.display = "block";
                        responsiveCoMa.style.display = "block";
                        responsiveCoMcp.style.display = "block";
                        responsiveCoNav.style.display = "block";
                        responsiveCoNavLeft.style.width = 220 + 'px';

                        responsiveCoNavLeft.onmouseover = function () {
                            this.style.width = 220 + 'px';
                        }
                        responsiveCoNavLeft.onmouseout = function () {
                            this.style.width = 220 + 'px';
                        }
                    }
                }

                var x = window.matchMedia("(max-width: 1366px)")
                myFunction(x)
                x.addListener(myFunction)
            </script>

        <?php else : ?>

            <!-- --------------------------------------------- -->
            <!--Partie gauche pour les personnes non-connectés -->
            <!-- --------------------------------------------- -->
            <script>
                var responsiveNotCoUl   = document.getElementById('responsive-Not-Co-UL')
                var responsiveNotCoNoOption   = document.getElementById('responsiveNotCo')
                var responsiveNotCoDiaNav   = document.getElementById('responsivediaNav')
                var responsiveNotCoImg   = document.getElementById('responsiveNotCoImg')
                var responsiveNotCoNavLeft   = document.getElementById('responsiveNavLeft')

                function myFunction(x) {
                    if (x.matches) {
                        /** si l fenêtre fait moins de 1366px:
                         * pas de couleur blanche dans les ul
                         * display des li sur none
                         * et display de l'image sur block
                         * taille de la div a 45px */
                        responsiveNotCoUl.style.backgroundColor = "unset";
                        responsiveNotCoNoOption.style.display = "none";
                        responsiveNotCoDiaNav.style.display = "none";
                        responsiveNotCoImg.style.display = "block";
                        responsiveNotCoNavLeft.style.width = 45 + 'px';

                        // au survol de la barre de gauche
                        responsiveNotCoNavLeft.onmouseover = function () {
                            /** au survol de la souris
                             * la barre reprend ses 220 de pixels
                             * background des ul en blanc
                             * les textes des li sont affichés en block
                             * display de l'image sur none */
                            this.style.width = 220 + 'px';
                            responsiveNotCoUl.style.backgroundColor = "#fff"
                            responsiveNotCoNoOption.style.display = "block";
                            responsiveNotCoDiaNav.style.display = 'block';
                            responsiveNotCoImg.style.display = "none";
                        }
                        // Quand la souris ne survol plus la barre de gauche
                        responsiveNotCoNavLeft.onmouseout = function () {
                            /** quand la souris quitte le hover ou après clic sur un lien
                             * la couleurs et les li disparaissent
                             * et l'image reviens en display block*/
                            this.style.width = 45 + 'px';
                            responsiveNotCoUl.style.backgroundColor = "unset";
                            responsiveNotCoNoOption.style.display = "none";
                            responsiveNotCoDiaNav.style.display = "none";
                            responsiveNotCoImg.style.display = "block";
                        }

                    } else {
                        // si la taille de la fenêtre est supérieur à 1366px
                        /** background blanc dans les ul
                         * affichage li en block
                         * image sur display none
                         * taille de la barre de gauche à 220 px, et hover ou non la taille reste à 220px */
                        responsiveNotCoUl.style.backgroundColor = "#fff"
                        responsiveNotCoNoOption.style.display = "block";
                        responsiveNotCoDiaNav.style.display = "block";
                        responsiveNotCoImg.style.display = "none";
                        responsiveNotCoNavLeft.style.width = 220 + 'px';

                        responsiveNotCoNavLeft.onmouseover = function () {
                            this.style.width = 220 + 'px';
                        }
                        responsiveNotCoNavLeft.onmouseout = function () {
                            this.style.width = 220 + 'px';
                        }
                    }
                }
                var x = window.matchMedia("(max-width: 1366px)")
                myFunction(x)
                x.addListener(myFunction)

            </script>
        <?php endif; ?>
</header>
