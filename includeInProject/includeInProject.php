<?php
session_start();
if (isset($_SESSION['id'])) {
    $sessionID = $_SESSION['id'];
} else {
    header("location: ../index.php");
    exit();
}
require "../sql/connexion.php";
?>
    <!doctype html>
    <html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="../css/style.css">
        <title>Nouvelle catégorie de projet</title>
    </head>
    <body>
    <?php
    require '../navigation/top-left-nav.php';
    ?>
    <main>
        <div class="index_col_center">

            <h2>Nouvelle catégorie</h2>
            <?php if ($_SESSION['role'] != 1): ?>
                <div> Vous n'êtes pas autorisé à ajouter dans cette partie de GRAF</div>
            <?php else: ?>
                <form action="check-add-new-category.php" method="post">
                    <label for="cat_project">Nouvelle catégorie de projet:</label>
                    <input type="text" name="cat_project" id="cat_project"> <br> <br>
                    <label for="color">Entre une couleur</label>
                    <input type="color" id="color" name="color"> <br><br>
                    <input type="submit" name="envoyer" value="Enregistrer">
                </form>
                <br>
                <?php if (isset($_SESSION['errors'])): ?>
                    <div class="error"><?= implode('<br>', $_SESSION['errors']) ?></div>
                <?php endif; ?>

                <?php
                $reqCats = $dbh->prepare("SELECT * FROM includeInProjects");
                $reqCats->execute();
                ?>
                <div class="addCategories"><b>Les différentes catégories de projet sont:</b><br>
                    <table class="responsive-includeinproject">
                        <?php while ($cats = $reqCats->fetch()): ?>
                            <tr>
                                <td><?= $cats['name']; ?></td>
                                <td style="background-color:<?= $cats["color"]; ?>; width: 25px"></td>
                            </tr>
                        <?php endwhile; ?>
                    </table>
                </div>
            <?php endif; ?>
        </div>
    </main>
    </body>
    </html>
<?php unset($_SESSION['errors']); ?>