<?php
session_start();

require "../sql/connexion.php";

$error = [];

if (isset($_POST['envoyer'])) {
    $cat_project = htmlspecialchars($_POST['cat_project']);
    $color = $_POST['color'];
    if (!empty($cat_project)) {
        if (preg_match("/^[\-a-zA-Z0-9éèêëàçùôö' ]+$/", $cat_project)) {
            if (!empty($color)) {
                $checkColors = $dbh->prepare("SELECT * FROM includeInProjects WHERE color = ?");
                $checkColors->execute(array(
                    $color,
                    ));
                $checkColor = $checkColors->rowCount();
                if ($checkColor == 0) {
                    if ($color != '#000000' and $color != '#ffffff') {
                        if (preg_match('/^#[a-f0-9]{6}$/i', $color)) {
                            $includeInProject = $dbh->prepare("INSERT INTO includeInProjects (name, color) VALUES (?,?)");
                            $includeInProject->execute(array(
                                $cat_project,
                                $color,
                            ));
                            header('location:../redirection/newcategorie.php');
                            exit();
                        } else {
                            $error[] = "Erreur: La couleur dois être au format hexadecimal.";
                        }
                    } else {
                        $error[] = "Erreur: Attention vous n'ête pas autorisé à sélectionner les couleur noir et blanche";
                    }
                } else {
                    $error[] = "Erreur: Attention la couleur que vous avez sélectionné est déjà utilisée" . $checkColor;
                }
            } else {
                $error[] = "Erreur: Aucune couleur n'a été sélectionnée ";
            }
        } else {
            $error[] = "Erreur: Attention vous entré un symbole non-autorisé";
        }
    } else {
        $error[] = "Erreur: Attention vous n'avez pas entré un nom pour cette nouvelle catégorie.";
    }
}

if (!empty($error)) {
    $_SESSION['errors'] = $error;
    header('location: includeInProject.php');
    exit();
}