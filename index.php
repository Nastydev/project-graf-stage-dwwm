<?php
session_start();
if (isset($_SESSION['id'])) {
    $sessionID = $_SESSION['id'];
} else {
    $sessionID = NULL;
}

# ***** AVERTISSEMENT SUR LA LICENSE *****
# Copyleft Tourneux Cédric, 2021
#
# Version: 1.0
#
# Adresse mail : cedric.tourneux@gmail.com
#
# Ce logiciel est un programme informatique servant à la gestion
# du Reste à faire, remplacents le fichier Excel.
#
# Ce logiciel est régi par la licence CeCILL soumise au droit français et
# respectant les principes de diffusion des logiciels libres. Vous pouvez
# utiliser, modifier et/ou redistribuer ce programme sous les conditions
# de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
# sur le site "http://www.cecill.info".
#
# En contrepartie de l'accessibilité au code source et des droits de copie,
# de modification et de redistribution accordés par cette licence, il n'est
# offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
# seule une responsabilité restreinte pèse sur l'auteur du programme,  le
# titulaire des droits patrimoniaux et les concédants successifs.
#
# A cet égard  l'attention de l'utilisateur est attirée sur les risques
# associés au chargement,  à l'utilisation,  à la modification et/ou au
# développement et à la reproduction du logiciel par l'utilisateur étant
# donné sa spécificité de logiciel libre, qui peut le rendre complexe à
# manipuler et qui le réserve donc à des développeurs et des professionnels
# avertis possédant  des  connaissances  informatiques approfondies.  Les
# utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
# logiciel à leurs besoins dans des conditions permettant d'assurer la
# sécurité de leurs systèmes et ou de leurs données et, plus généralement,
# à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
#
# Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
# pris connaissance de la licence CeCILL, et que vous en avez accepté les
# termes.
# ***** AVERTISSEMENT SUR LA LICENSE *****
?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="#">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="jquery-ui/jquery-ui.css" media="screen"/>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
          integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <!-- JS -->
    <script type="text/javascript" src="js/jQuery.js"></script>
    <script type="text/javascript" src="jquery-ui/jquery-ui.js"></script>
    <title>GRAF</title>
</head>
<body>

<?php
require 'navigation/top-left-nav.php';
require "sql/connexion.php";
?>
<main>
    <div class="index_col_center">
        <button class="tablink" onclick="openPage('RAF', this, '#fff')" id="defaultOpen">Reste à faire</button>
        <button class="tablink" onclick="openPage('GDR', this, '#fff')" id="gdr">Gestion du risque</button>
        <button class="tablink" onclick="openPage('ODC', this, '#fff')" id="odc">Outils de communication</button>

        <div id="RAF" class="tabcontent">
            <h2><a href="index.php">Reste A Faire</a></h2>
            <p id="color-priority">Signification contours:</p>
            <div class="color-priority">
                <div>Priorité 1 :
                    <div class="addColor" style="background-color:green"></div>
                </div>
                <div>Priorité 2 :
                    <div class="addColor" style="background-color:yellow"></div>
                </div>
                <div>Priorité 3 :
                    <div class="addColor" style="background-color:darkorange"></div>
                </div>
                <div>Priorité 4 :
                    <div class="addColor" style="background-color:red"></div>
                </div>
            </div>
            <?php include 'index/card-raf.php' ?>
        </div>

        <div id="GDR" class="tabcontent">
            <h2>Gestion du risque</h2>
            <p>...</p>
        </div>

        <div id="ODC" class="tabcontent">
            <h2>Gestion des outils de communication</h2>
            <p>...</p>

        </div>

        <script>
            function openPage(pageName, elmnt, color) {
                var i, tabcontent, tablinks;
                tabcontent = document.getElementsByClassName("tabcontent");
                for (i = 0; i < tabcontent.length; i++) {
                    tabcontent[i].style.display = "none";
                }
                tablinks = document.getElementsByClassName("tablink");
                for (i = 0; i < tablinks.length; i++) {
                    tablinks[i].style.backgroundColor = "";
                    tablinks[i].style.borderBottom = "";
                    tablinks[i].style.color = "#fff";

                }
                document.getElementById(pageName).style.display = "block";
                elmnt.style.backgroundColor = color;
                elmnt.style.borderBottom = "thin solid #0066ff";
                elmnt.style.color = "#0066ff";
            }

            // Get the element with id="defaultOpen" and click on it
            document.getElementById("defaultOpen").click();


            // --------- changement des noms dans les onglets en dessous de 1024px de largeur de l'écran ----------
            let raf = document.getElementById("defaultOpen");
            let gdr = document.getElementById("gdr");
            let odc = document.getElementById("odc");

            function matches() {
                raf.innerHTML = window.matchMedia("(max-width: 1024px)").matches ? "RAF" : "Reste à faire";
                gdr.innerHTML = window.matchMedia("(max-width: 1024px)").matches ? "Risque" : "Gestion du risque";
                odc.innerHTML = window.matchMedia("(max-width: 1024px)").matches ? "Communication" : "Outils de communication";
            }

            window.onresize = function (event) {
                matches();
            };
            matches();

        </script>
    </div>
</main>
</body>
</html>