<?php
session_start();
if (isset($_SESSION['id'])) {
    $sessionID = $_SESSION['id'];
    header("location: ../index.php");
    exit();
}
require '../sql/connexion.php';

if (isset($_POST['connexion'])) {
    $username = htmlentities($_POST['username-mail']);
    $password = htmlentities($_POST['password']);


    $reqPseudo = $dbh->prepare('SELECT * FROM membre WHERE pseudo = ?'); //récupération du pseudo pour vérifier si il existe
    $reqPseudo->execute(array(
        $username,
    ));
    $pseudoExist = $reqPseudo->rowCount();

    $reqmail = $dbh->prepare('SELECT * FROM membre WHERE mail = ?'); //récupération du mail pour vérifier si il existe
    $reqmail->execute(array(
        $username,
    ));
    $mailexist = $reqmail->rowCount();

    $reqPassword = $dbh->prepare("SELECT * FROM membre WHERE mail = ? OR pseudo = ?"); //recupération du mot de passe
    $reqPassword->execute(array(
        $username,
        $username,
    ));
    $user = $reqPassword->fetch();

// si input rempli et egale mail ou pseudo, on continue
    if (!empty($mailexist == 1) or !empty($pseudoExist == 1)) {
        if (!empty(password_verify($password, $user['password']))) {

            $authorId = $user['author_id'];
            $reqNameAuthor = $dbh->prepare("SELECT * FROM author WHERE id = ?");
            $reqNameAuthor->execute(array(
                $authorId,
            ));
            $authName = $reqNameAuthor->fetch();
            $_SESSION['id'] = md5(time() . mt_rand(1,10000000));
            $_SESSION['authorId'] = $user['id'];
            $_SESSION['authorName'] = $authName['name'];
            $_SESSION['role'] = $user['roles_id'];

            header("location: ../index.php");
            exit;

        } else {
            $error = "Le mot de passe ne correspond pas";
        }
    } else {
        $error = "Un des champs n'est pas renseigné";
    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../css/style.css">
    <title>Document</title>
</head>
<body>
<?php require('../navigation/top-left-nav.php'); ?>
<main>
    <div class="index_col_center">
        <form action="" method="post" name="login">
            <h2 class="box-title">Connexion</h2>

            <div>
                <label for="username-mail"></label> <br>
                <input type="text" name="username-mail" id="username-mail"
                       placeholder="Votre pseudo ou votre mail">
            </div>
            <div>
                <label for="password"></label>
                <input type="password" id="password" name="password" placeholder="Mot de passe">
            </div>
            <div>
                <input type="submit" value="Connexion " name="connexion">
            </div>
            <p class="box-register">Vous êtes nouveau ici? <a href="form-inscription.php" id="inscriptionLink">S'inscrire</a>
            </p>

            <?php if (isset($error)) { ?>
                <div class="error">
                    <p class="errorMessage"><?php echo $error; ?></p>
                </div>
            <?php } ?>

        </form>
    </div>
</main>
</body>
</html>