<?php
session_start();
if (isset($_SESSION['id'])) {
    $sessionID = $_SESSION['id'];
    header("location: ../index.php");
    exit();
}
require "../sql/connexion.php";

if (isset($_POST["inscription-membres"])) {
    //verification que les champs ne sont pas vides et qu'ils comportent bien des lettres et des chiffres sans esapces
    if (
        !empty($_POST["pseudo"])
        and !empty($_POST["author"])
        and !empty($_POST["mail"])
        and !empty($_POST["mail2"])
        and !empty($_POST["password"])
        and !empty($_POST["password2"])) {
        /**  fonction pour vérifier:
         * stripslashes () = Supprime les antislashs d'une chaîne
         * htmlspecialchars =  Convertit les caractères spéciaux en entités HTML
         */
        function validation($donnees)
        {
            $donnees = stripslashes($donnees);
            $donnees = htmlspecialchars($donnees);
            return $donnees;
        }

        $pseudo = validation($_POST["pseudo"]);
        $author = validation($_POST["author"]);
        $mail = validation($_POST["mail"]);
        $mail2 = validation($_POST["mail2"]);

        $password = password_hash($_POST["password"], PASSWORD_DEFAULT);
        $password2 = password_verify($_POST["password2"], $password);

        //vérification que pseudo et auteur comporte que des lettre et chiffre
        if (preg_match("/^[a-zA-Z0-9 ]+$/", $pseudo)
            and preg_match("/^[a-zA-Z0-9 ]+$/", $author)) {
            //vérification que le mot de passe ne comporte au moins majuscule et un chiffre et soit de 6 caractères minimum
            if (preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$/", $_POST['password'])) {
                //vérification que le pseudo n'existe pas déjà dans la base de donnée
                $reqPseudo = $dbh->prepare('SELECT * FROM membre WHERE pseudo = ?');
                $reqPseudo->execute(array(
                    $pseudo,
                ));
                $pseudoExist = $reqPseudo->rowCount();
                if ($pseudoExist == 0) {
                    //vérification que la taille du pseudo et de l'auteur ne dépasse pas 255 caractères
                    $pseudoLength = strlen($pseudo);
                    $authorLength = strlen($author);
                    if ($pseudoLength <= 255 and $authorLength <= 255) {
                        //vérification que mail et mail2 sont identiques
                        if ($mail == $mail2) {
                            //vérification que mail est bien un EMAIL !
                            if (filter_var($mail, FILTER_VALIDATE_EMAIL)) {
                                //vérification que le mail n'est pas déjà présent dans la base de donnée
                                $reqmail = $dbh->prepare('SELECT * FROM membre WHERE mail = ?');
                                $reqmail->execute(array(
                                    $mail,
                                ));
                                $mailexist = $reqmail->rowCount();
                                if ($mailexist == 0) {
                                    //vérification que les mots de passe sont identiques
                                    if ($password == $password2) {
                                        //ajout du nom d'auteur dans la table author
                                        $insertAuth = $dbh->prepare("INSERT INTO author (name) VALUE (?)");
                                        $insertAuth->execute(array(
                                            $author,
                                        ));
                                        //récupération de l'id de l'ateur add
                                        $reqAuthor = $dbh->lastInsertId();
                                        // de base il aura le role "non-autorisé
                                        $notAllowed = 3;
                                        //ajout du membre si tout c'est bien passé
                                        $insertmbr = $dbh->prepare("INSERT INTO membre (author_id, roles_id, pseudo, mail, password) VALUE (?,?, ?, ?, ?)");
                                        $insertmbr->execute(array(
                                            $reqAuthor,
                                            $notAllowed,
                                            $pseudo,
                                            $mail,
                                            $password,
                                        ));

                                        $_SESSION['register'] = 1;

                                        header("location: ../redirection/redirection-inscription.php");
                                        exit;


                                    } else {

                                        $error = "Vos mots de passe ne correspond pas";
                                    }
                                } else {

                                    $error = "Adresse email déjà utilisé";
                                }
                            } else {
                                $error = "Votre adresses mail n'est pas valide";
                            }
                        } else {

                            $error = "Vos adresses mail ne correspond pas";
                        }
                    } else {

                        $error = "Votre pseudo ou le nom d'auteur est/sont trop long";
                    }

                } else {

                    $error = "Ce pseudo exist déjà";
                }

            } else {
                $error = "Veuillez respecter: Au moins une majuscule, un chiffre, une minuscule et 6 cactères minimum.";
            }

        } else {
            $error = "Ajout de caractère(s) non autorisé(s)";
        }

    } else {

        $error = "Tous les champs doivent être remplis";
    }

}

?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
          integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

    <title>Inscription</title>
</head>
<body>
<?php
require "../navigation/top-left-nav.php"; ?>
<main>
    <div class="index_col_center">
        <h2>Inscription</h2>

        <form action="" method="post">
            <table class="table-center">
                <tr>
                    <td><label class="label-form-mbr" for="pseudo">Pseudo: </label></td>
                    <td><input type="text" name="pseudo" id="pseudo" placeholder="Votre pseudo"></td>
                </tr>
                <tr>
                    <td><label for="author">Auteur: </label></td>
                    <td><input type="text" name="author" id="author" placeholder="Votre nom d'auteur"></td>
                </tr>
                <tr>
                    <td><label for="mail">Mail: </label></td>
                    <td><input type="email" name="mail" id="mail" placeholder="Votre adresse mail"></td>
                </tr>
                <tr>
                    <td><label for="mail2">Confirmation du mail: </label></td>
                    <td><input type="email" name="mail2" id="mail2" placeholder="Confirmez votre adresse mail"></td>
                </tr>
                <tr>
                    <td><label for="password">Mot de passe <br>(6 caractère minimum. <br> 1 Lettre majuscule & 1
                            chiffre):
                        </label></td>
                    <td><input type="password" name="password" id="password" placeholder="Votre mot de passe"></td>
                </tr>
                <tr>
                    <td><label for="password2">Confirmation mot de passe: </label></td>
                    <td><input type="password" name="password2" id="password2"
                               placeholder="confirmez votre mot de passe">
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <input type="submit" value="Inscription" name="inscription-membres">
        </form>
        <br>
        <br>
        <?php
        if (isset($error)) {
            echo "<div class='error'>";
            echo "<i class='fas fa-exclamation-triangle'></i>" . "  " . $error . '  ' . "<i class='fas fa-exclamation-triangle'></i>";
            echo '</div>';
        }
        ?>

    </div>
</main>
</body>
</html>
