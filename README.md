### SQL connexion, importation & mise à place.

- Créer fichier de config dans le dossier sql (exemple config_ex.php)
- Importer la database graf.sql ce trouvant dans le dossier sql/

### Important phpmyadmin: 
- table role:

        id        role  
        1         Administrateur  
        2         Autorisé  
        3         Non-autorisé  

- table includeInProjects: 

        id      name        color  
        1       aucune      #000000    


